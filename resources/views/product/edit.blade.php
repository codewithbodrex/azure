@extends('layout.master');

@section('judul')
    Edit Product
@endsection

@section('content')
    <form action="/product/{{$product->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama Product</label>
            <input type="text" class="form-control"name="nama" value="{{$product->nama}}">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror        
        <div class="form-group">
            <label>Harga</label>
            <input type="number" class="form-control"name="harga" min="1" value="{{$product->harga}}">
        </div>
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
        <div class="form-group">
            <label>Stock</label>
            <input type="number" class="form-control"name="stock" min="1" value="{{$product->stock}}">
        </div>
        @error('stock')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" class="form-control" cols="30" rows="10">{{$product->deskripsi}}</textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror                        
        <button type="submit" class="btn btn-primary">Update</button>
  </form>

@endsection