@extends('layout.master');

@section('judul')
    Detail Product
@endsection

@section('content')
    <h1>{{$product->nama}}</h1>
    <p>{{$product->deskripsi}} </p>

    <a href="/product" class="btn btn-secondary">Back</a>
@endsection