@extends('layout.master');

@section('judul')
    Daftar Produk
@endsection

@section('content')
    <a href="/product/create" class="btn btn-primary btn-sm">Tambah</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama Produk</th>
            <th scope="col">Harga</th>
            <th scope="col">Stock</th>
            <th scope="col">Action</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($product as $key => $value) <!--Associative array-->
                <tr>
                    <th scope="row">{{$key + 1}}</th>
                    <td>{{$value -> nama}}</td>
                    <td>{{$value -> harga}}</td>
                    <td>{{$value -> stock}}</td>
                    <td>
                        <form action="/product/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/product/{{$value->id}}" class="btn btn-primary btn-sm">Detail</a>
                            <a href="/product/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                        </form>

                    </td>

                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
      </table>

@endsection
