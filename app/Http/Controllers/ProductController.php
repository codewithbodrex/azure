<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


use Illuminate\Http\Request;

class ProductController extends Controller
{
    //
    public function index(){
        $product = DB::table('product')
        ->get();
        return view('product.tampil',['product' => $product]);
    }
    public function create(){
        return view('product.tambah');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stock' => 'required',
            'deskripsi' => 'required'
        ]);
        DB::table('product')->insert([
            'nama' => $request['nama'],
            'harga'  => $request['harga'],
            'stock'  => $request['stock'],
            'deskripsi'  => $request['deskripsi']
        ]);
        return redirect('/product');        
    }   
    public function detail($id){
        $product = DB::table('product')
        ->where('product.id', $id)
        ->first();
        return view('product.detail' ,['product' => $product]);
    }
    public function edit($id){
        $product = DB::table('product')
        ->where('id', $id)->first();
        return view('product.edit',['product' => $product]);
    }        
    public function update(Request $request ,$id){
        $affected = DB::table('product')
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request->nama,
                    'harga' => $request->harga,
                    'stock' => $request->stock,
                    'deskripsi' => $request->deskripsi
                ]
            );
        return redirect('/product');
    }

    public function destroy($id){
        $deleted = DB::table('product')
        ->where('id', $id)->delete();
        $affected = DB::table('product')
              ->update(
                [
                    'id' => 1,
                ]
            );        
        return redirect('/product');
    }         
}
