<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\ProductController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);

Route::get('/data', [DataController::class, 'index']);
Route::get('/data/create', [DataController::class,'create']);
Route::post('/data', [DataController::class,'store']);



Route::get('/product', [ProductController::class,'index']);
Route::get('/product/create', [ProductController::class,'create']);
Route::post('/product', [ProductController::class,'store']);

Route::get('/product/{edit_id}', [ProductController::class,'detail']);
Route::get('/product/{edit_id}/edit', [ProductController::class,'edit']);
Route::put('/product/{edit_id}', [ProductController::class,'update']);

Route::delete('/product/{edit_id}', [ProductController::class,'destroy']);

Route::get('/calculator', function(){
    return view('calc.calculator');
});


