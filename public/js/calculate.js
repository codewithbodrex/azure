let display = document.getElementById('display');

let button = Array.from(document.getElementsByClassName('num-btn'));
let operator = Array.from(document.getElementsByClassName('operator'));
let equal = Array.from(document.getElementsByClassName('equal-sign'));

let num_1 = '';
let num_2 = '';
let sign;

button.map(button =>{
    button.addEventListener('click' , (e) =>{
        switch(e.target.value){
            case '.':
                if(!display.value.includes('.') && display.value != ''){
                    display.value += e.target.value;
                }
                break;
            case '<-':
                if(display.value){
                    var text = display.value;
                    text = text.slice(0,-1);
                    display.value = text;
                    break;
                }
            default:
                display.value += e.target.value;
                break;
        };
    });
});

operator.map(operator =>{
    operator.addEventListener('click' , (e) =>{
        switch(e.target.value){
            case 'AC':
                display.value = '';
                num_1 = '';
                num_2 = '';
                break;
            default:
                sign = e.target.value;
                if(display.value.includes('.')){
                    num_1 = parseFloat(display.value);
                }else{
                    num_1 = parseInt(display.value);
                }
                display.value = '';
                display.placeholder = 'Insert a number ...';
                break;                                
        };
    });
});

equal.map(equal =>{
    equal.addEventListener('click' , (e) =>{
        if(num_1 == '' && sign == ''){
            display.value = '';
            display.placeholder = 'Error! need number and operation';
        }else{
            if(display.value.includes('.')){
                num_2 = parseFloat(display.value);
            }else{
                num_2 = parseInt(display.value);
            }
            display.value = '';
            switch(sign){
                case'*':
                    display.value += num_1*num_2;
                    break;
                case '/':
                    display.value += num_1/num_2;
                    break;
                case '+':
                    display.value += num_1+num_2;
                    break;
                case '-':
                    display.value += num_1-num_2;
                    break;
            };
        };
        num_1 = '';
        num_2 = '';
        sign = '';
    });
});