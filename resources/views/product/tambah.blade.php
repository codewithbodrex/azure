@extends('layout.master');

@section('judul')
    Tambah Product
@endsection

@section('content')
    <form action="/product" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control"name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror        
        <div class="form-group">
            <label>Harga</label>
            <input type="number" class="form-control"name="harga" min="1">
        </div>
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
        <div class="form-group">
            <label>Stock</label>
            <input type="number" class="form-control"name="stock" min="1">
        </div>
        @error('stock')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('deskripsi')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror                        
        <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection