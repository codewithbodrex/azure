<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;


use Illuminate\Http\Request;

class DataController extends Controller
{
    //
    public function index(){
        $data = DB::table('data')
        ->orderBy('harga', 'asc')
        ->orderBy('rating', 'desc')
        ->orderBy('likes', 'desc')
        ->get();
        return view('data.data',['data' => $data]);
    }
    public function create(){
        return view('data.tambah');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'harga' => 'required',
            'rating' => 'required',
            'likes' => 'required'
        ]);
        DB::table('data')->insert([
            'nama' => $request['nama'],
            'harga'  => $request['harga'],
            'rating'  => $request['rating'],
            'likes'  => $request['likes']
        ]);
        return redirect('/data');        
    }
    
}
