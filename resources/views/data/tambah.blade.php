@extends('layout.master');

@section('judul')
    Tambah Data
@endsection

@section('content')
    <form action="/data" method="POST">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control"name="nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror        
        <div class="form-group">
            <label>Harga</label>
            <input type="number" class="form-control"name="harga" min="1">
        </div>
        @error('harga')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror 
        <div class="form-group">
            <label>Rating</label>
            <input type="text" class="form-control"name="rating">
        </div>
        @error('rating')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
            <label>Likes</label>
            <input type="number" class="form-control"name="likes" min="1">
        </div>
        @error('likes')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror                        
        <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection