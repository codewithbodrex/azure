@extends('layout.master')

@section('judul')
    Calculator Page
@endsection

@push('scripts')
    <script src="{{asset('/js/calculate.js')}}"></script>
@endpush

@section('content')
<div class="container">
    <div class="col">
      <div class="row p-2">
        <input type="text" class="form-control" placeholder="Insert a number ..." id="display" disabled style="width: 100%; height: 60px;">
      </div>
      <div class="row p-2">
        <button type="button" class="operator btn btn-info mx-1 shadow" value="+" style="width: 100px; height: 80px;">+</button>
        <button type="button" class="operator btn btn-info mx-1 shadow" value="-" style="width: 100px; height: 80px;">-</button>
        <button type="button" class="operator btn btn-info mx-1 shadow" value="*" style="width: 100px; height: 80px;">&times;</button>
        <button type="button" class="operator btn btn-info mx-1 shadow" value="/" style="width: 100px; height: 80px;">&divide;</button>
      </div>
      <div class="row p-2">
        <button type="button" value="7" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">7</button>
        <button type="button" value="8" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">8</button>
        <button type="button" value="9" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">9</button>
      </div>
      <div class="row p-2">
        <button type="button" value="4" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">4</button>
        <button type="button" value="5" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">5</button>
        <button type="button" value="6" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">6</button>
      </div>
      <div class="row p-2">
        <button type="button" value="1" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">1</button>
        <button type="button" value="2" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">2</button>
        <button type="button" value="3" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">3</button>
        <button type="button" value="<-" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;"><i class="fas fa-backspace"></i></button>
      </div>
      <div class="row p-2">
        <button type="button" class="all-clear operator function btn btn-danger btn-sm mx-1 shadow" value="AC" style="width: 100px; height: 80px;">AC</button>        
        <button type="button" value="0" class="num-btn btn btn-light shadow mx-1" style="width: 100px; height: 80px;">0</button>        
        <button type="button" class="num-btn decimal function btn btn-secondary mx-1 shadow" value="." style="width: 100px; height: 80px;">.</button>


        <button type="button" class="equal-sign  btn btn-default mx-1 shadow" value="=" style="width: 100px; height: 80px;">=</button>
      </div>            
    </div>
  </div>
@endsection