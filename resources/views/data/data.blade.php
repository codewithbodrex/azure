@extends('layout.master');

@section('judul')
    Selamat Datang Page Data
@endsection

{{-- @push('scripts')
{{-- <script>
    Swal.fire({
        title: "Berhasil!",
        text: "Memasangkan script sweet alert",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script> --}}
   
{{-- @endpush --}}
@section('content')
    <a href="/data/create" class="btn btn-primary btn-sm mx-2">Tambah</a>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Nama</th>
            <th scope="col">Harga</th>
            <th scope="col">Rating</th>
            <th scope="col">Likes</th>
          </tr>
        </thead>
        <tbody>
            @forelse ($data as $key => $value) <!--Associative array-->
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value -> nama}}</td>
                    <td>{{$value -> harga}}</td>
                    <td>{{$value -> rating}}</td>
                    <td>{{$value -> likes}}</td>

                </tr>
            @empty
                <tr>
                    <td>Tidak ada data</td>
                </tr>
            @endforelse
        </tbody>
      </table>


@endsection